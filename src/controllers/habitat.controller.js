const Habitat = require("./../models/habitat.model")

//GET
const getAllHabitats = async (req,res,next) =>{
    try{
        const habitats = await Animal.find();
        return res.status(200).json(habitats);
    }catch(error){
        return next(error);
    };
};

const getHabitatById = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const findHabitat = await Habitat.findById(id)
        return res.status(200).json(findHabitat)
    }catch(error){
        return next(error)
    }
};

//POST
const postHabitat = async (req,res,next) => {
    try{
        const newHabitat = new Habitat(req.body);
        const newHabitatInBd = await newHabitat.save();
        return res.status(201).json(newHabitatInBd)
    }catch(error){
        return next(error)
    }
};

//PUT
const putHabitat = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const newHabitat = new Habitat(req.body);
        newHabitat._id = id;
        const habitatUpdated = await Habitat.findByIdAndUpdate(id, newHabitat);
        return res.status(200).json(habitatUpdated)
    }catch(error){
        return next(error)
    }
}

//DELETE
const deleteHabitat = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const deletedHabitat = await Habitat.findByIdAndDelete(id);
        return res.status(200).json(deletedHabitat)
    }catch(error){
        return next(error)
    }
}


module.exports = {
    getAllHabitats,
    getHabitatById,
    postHabitat,
    putHabitat,
    deleteHabitat
}