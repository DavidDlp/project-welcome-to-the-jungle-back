const Family = require("./../models/families.model")

//GET
const getAllFamilies = async (req,res,next) =>{
    try{
        const families = await Family.find();
        return res.status(200).json(families);
    }catch(error){
        return next(error);
    };
};

const getFamilyById = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const findFamily = await Family.findById(id)
        return res.status(200).json(findFamily)
    }catch(error){
        return next(error)
    }
};

//POST
const postFamily = async (req,res,next) => {
    try{
        const newFamily = new Family(req.body);
        const newFamilyInBd = await newFamily.save();
        return res.status(201).json(newFamilyInBd)
    }catch(error){
        return next(error)
    }
};

//PUT
const putFamily = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const newFamily = new Family(req.body);
        newFamily._id = id;
        const familyUpdated = await Family.findByIdAndUpdate(id, newFamily);
        return res.status(200).json(familyUpdated)
    }catch(error){
        return next(error)
    }
};

//PATCH
const patchHabitatInFamily = async (req, res, next) =>{
    try{
        const {id} = req.params;
        const idHabitat = req.body.idHabitat;
        const updateHabitatInFamily = await Pets.findByIdAndUpdate(id,{$push:{family:idHabitat}});
        return res.status(200).json(updateHabitatInFamily)

    }catch(error){
        return next(error)
    }
};

//DELETE
const deleteFamily = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const deletedFamily = await Family.findByIdAndDelete(id);
        return res.status(200).json(deletedFamily)
    }catch(error){
        return next(error)
    }
}


module.exports = {
    getAllFamilies,
    getFamilyById,
    postFamily,
    putFamily,
    patchHabitatInFamily,
    deleteFamily
}