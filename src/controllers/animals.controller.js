const Animal = require("./../models/animals.model")

//GET
const getAllAnimals = async (req,res,next) =>{
    try{
        const animals = await Animal.find().populate('family');
        return res.status(200).json(animals);
    }catch(error){
        return next(error);
    };
};

const getAnimalById = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const findAnimal = await Animal.findById(id).populate('family');
        return res.status(200).json(findAnimal);
    }catch(error){
        return next(error)
    }
};

//POST
const postAnimal = async (req,res,next) => {
    try{
        const newAnimal = new Animal(req.body);
        const newAnimalInBd = await newAnimal.save();
        return res.status(201).json(newAnimalInBd)
    }catch(error){
        return next(error)
    }
};

//PUT
const putAnimal = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const newAnimal = new Animal(req.body);
        newAnimal._id = id;
        const animalUpdated = await Animal.findByIdAndUpdate(id, newAnimal);
        return res.status(200).json(animalUpdated)
    }catch(error){
        return next(error)
    }
}
//PATCH
const patchFamilyInAnimal = async (req, res, next) =>{
    try{
        const {id} = req.params;
        const idFamily = req.body.idFamily;
        const updateFamilyInAnimal = await Animal.findByIdAndUpdate(id,{$push:{family:idFamily}});
        return res.status(200).json(updateFamilyInAnimal)

    }catch(error){
        return next(error)
    }
};

//DELETE
const deleteAnimal = async (req,res,next) =>{
    try{
        const {id} = req.params;
        const deletedAnimal = await Animal.findByIdAndDelete(id);
        return res.status(200).json(deletedAnimal)
    }catch(error){
        return next(error)
    }
}

module.exports = {
    getAllAnimals,
    getAnimalById,
    postAnimal,
    putAnimal,
    patchFamilyInAnimal,
    deleteAnimal
}