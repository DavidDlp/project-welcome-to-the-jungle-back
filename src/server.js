const express = require ('express')
const mongoose = require ('mongoose')
const morgan = require('morgan');
const cors = require("cors");
require('dotenv').config();

const animalsRoutes = require('./routes/animal.routes');
const familiesRoutes = require('./routes/families.routes');
const habitatRoutes = require('./routes/habitat.routes');

const app = express();
const PORT = process.env.PORT;

//MIDDLEWARES
app.use(cors({
  origin: '*',
  credentials: true,
  }));

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ROUTES
app.use("/animals", animalsRoutes);
app.use("/families", familiesRoutes);
app.use("/habitats", habitatRoutes);

app.use("*", (req, res, next) => {
    const error = new Error();
    error.status = 404;
    error.message = "Route not found";
    return next(error);
  });

//ERROR
app.use((error, req, res, next) => {
    return res
      .status(error.status || 500)
      .json(error.message || "Unexpected error");
  }); 

//CONECT DB
mongoose.connect(process.env.MONGODBURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() =>
    app.listen(PORT, () =>
      console.info(`Server is running in http://localhost:${PORT}`)
    )
  );