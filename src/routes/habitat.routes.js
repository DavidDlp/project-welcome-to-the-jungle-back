const express = require('express');
const { getAllHabitats, getHabitatById, postHabitat, putHabitat, deleteHabitat } = require('../controllers/habitat.controller');
const habitatRoutes = express.Router();

// habitatRoutes.get('/',(req,res) =>{
//     res.send('Habitats')
// });

//GET
habitatRoutes.get('/', getAllHabitats);
habitatRoutes.get('/:id', getHabitatById);
//POST
habitatRoutes.post('/addHabitat', postHabitat);
// //PUT
habitatRoutes.put('/updated/:id', putHabitat);
// //DELETE
habitatRoutes.delete('/deleted/:id', deleteHabitat);

module.exports = habitatRoutes