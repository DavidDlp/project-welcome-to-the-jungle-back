const express = require('express');
const { getAllAnimals, 
    getAnimalById, 
    postAnimal, 
    putAnimal,
    patchFamilyInAnimal, 
    deleteAnimal } = require('../controllers/animals.controller');
const animalsRoutes = express.Router();

// animalsRoutes.get('/',(req,res) =>{
//     res.send('Animals')
// });

//GET
animalsRoutes.get('/', getAllAnimals);
animalsRoutes.get('/:id', getAnimalById);
//POST
animalsRoutes.post('/addanimal', postAnimal);
//PUT
animalsRoutes.put('/:id', putAnimal);
//PATCH
animalsRoutes.patch('/addfamily/:id', patchFamilyInAnimal);
//DELETE
animalsRoutes.delete('/:id', deleteAnimal);

module.exports = animalsRoutes