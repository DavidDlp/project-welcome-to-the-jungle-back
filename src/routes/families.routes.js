const express = require('express');
const { getAllFamilies, 
    getFamilyById, 
    postFamily, 
    putFamily,
    patchHabitatInFamily, 
    deleteFamily } = require('../controllers/families.controller');
const familiesRoutes = express.Router();

// familiesRoutes.get('/',(req,res) =>{
//     res.send('Families')
// });

//GET
familiesRoutes.get('/', getAllFamilies);
familiesRoutes.get('/:id', getFamilyById);
//POST
familiesRoutes.post('/addFamily', postFamily);
//PUT
familiesRoutes.put('/updated/:id', putFamily);
//PATCH
familiesRoutes.patch('/addhabitat/:id', patchHabitatInFamily);
//DELETE
familiesRoutes.delete('/deleted/:id', deleteFamily);

module.exports = familiesRoutes