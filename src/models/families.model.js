const mongoose = require('mongoose');

const familySchema = new mongoose.Schema(
    {
        species: {type:String, required:true, trim:true},
        livingInGroup: {type:Boolean, required:true},
        habitat: {type:mongoose.Types.ObjectId, ref:'habitats'},
    },
    {
        timeStamps: true,
    }
)

const Family = mongoose.model('families', familySchema);

module.exports = Family