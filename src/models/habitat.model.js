const mongoose = require('mongoose');

const habitatSchema = new mongoose.Schema(
    {
        name: {type:String, required:true, trim:true},
        // location: ,
        mode: {enum:[
            'Tierra', 
            'Aire', 
            'Mar'
        ]},
    },
    {
        timeStamps: true,
    }
)

const Habitat = mongoose.model('habitats', habitatSchema);

module.exports = Habitat