const mongoose = require('mongoose');

const animalSchema = new mongoose.Schema(
    {
        name: {type:String, required:true, trim:true},
        carnivore: {type:Boolean, required:true},
        family: {type:mongoose.Types.ObjectId, ref:'families'},
    },
    {
        timeStamps: true,
    }
)

const Animal = mongoose.model('animals', animalSchema);

module.exports = Animal